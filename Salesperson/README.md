# Mage2 Module FactorBlue Salesperson

    ``factorblue/module-salesperson``

- [Main Functionalities](#markdown-header-main-functionalities)
- [Installation](#markdown-header-installation)
- [Configuration](#markdown-header-configuration)
- [Specifications](#markdown-header-specifications)
- [Attributes](#markdown-header-attributes)


## Main Functionalities
Enable logging in to VSF as a frontend customer from salesperson admin account

## Installation
\* = in production please use the `--keep-generated` option

### Zip file

- Unzip the zip file in `app/code/FactorBlue`
- Enable the module by running `php bin/magento module:enable FactorBlue_Salesperson`
- Apply database updates by running `php bin/magento setup:upgrade`\*
- Flush the cache by running `php bin/magento cache:flush`


## Configuration

- is_salesperson_login_enabled (salesperson/login_as_customer/is_salesperson_login_enabled)

- vsf_url (salesperson/login_as_customer/vsf_url)

- salesperson_role (salesperson/login_as_customer/salesperson_role)


## Specifications

- Admin customer grid is filtered by the representativeID defined both as a customer attribute and as an admin user field - the values should match
- Salesperson should be redirected to the VSF with the customer token generated once they click on the admin customer grid row
- Salesperson shouldn't be able to continue with the ordering process if the frontend customer account is not yet approved by Exact

## Attributes

- Adds "representive_id" attribute to the admin user DB table
