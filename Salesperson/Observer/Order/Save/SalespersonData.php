<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Observer\Order\Save;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use FactorBlue\Salesperson\Model\SalespersonDataObject;

class SalespersonData implements ObserverInterface
{
    /**
     * @var SalespersonDataObject
     */
    private $salespersonDataObject;

    /**
     * @param SalespersonDataObject $salespersonDataObject
     */
    public function __construct(
        SalespersonDataObject $salespersonDataObject
    ) {
        $this->salespersonDataObject = $salespersonDataObject;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $order = $observer->getData('order');

        if ($order) {
            $order->setData('placed_by_salesperson', $this->salespersonDataObject->getPlacedBySalesperson());
            $order->setData('salesperson_id', $this->salespersonDataObject->getSalespersonId());
        }
    }
}
