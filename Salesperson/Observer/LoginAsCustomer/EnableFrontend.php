<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Observer\LoginAsCustomer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\ActionInterface;

class EnableFrontend implements ObserverInterface
{
    const LOGIN_AS_CUSTOMER_ACTION_NAME = 'loginascustomer_login_index';

    /**
     * @var ActionFlag
     */
    private $actionFlag;

    /**
     * @param ActionFlag $actionFlag
     */
    public function __construct(
        ActionFlag $actionFlag
    ) {
        $this->actionFlag = $actionFlag;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $request = $observer->getRequest();
        $fullActionName = $request->getFullActionName();

        if ($fullActionName === self::LOGIN_AS_CUSTOMER_ACTION_NAME) {
            $this->actionFlag->set('', ActionInterface::FLAG_NO_DISPATCH, false);
        }
    }
}
