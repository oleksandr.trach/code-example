<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\ViewModel\Order\View;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Block\Adminhtml\Order\View\Tab\Info as OrderInfo;
use FactorBlue\Salesperson\Model\Order\PlacedBySalesperson\ValueResolver as PlacedBySalespersonValueResolver;

class SalespersonInformation implements ArgumentInterface
{
    /**
     * @var OrderInfo
     */
    private $orderInfo;

    /**
     * @var PlacedBySalespersonValueResolver
     */
    private $placedBySalespersonValueResolver;

    /**
     * @param OrderInfo $orderInfo
     * @param PlacedBySalespersonValueResolver $placedBySalespersonValueResolver
     */
    public function __construct(
        OrderInfo $orderInfo,
        PlacedBySalespersonValueResolver $placedBySalespersonValueResolver
    ) {
        $this->orderInfo = $orderInfo;
        $this->placedBySalespersonValueResolver = $placedBySalespersonValueResolver;
    }

    /**
     * @return string
     */
    public function getPlacedBySalesperson(): string
    {
        $order = $this->orderInfo->getOrder();
        $value = $order->getData('placed_by_salesperson');
        return $this->placedBySalespersonValueResolver->getValue($value);
    }

    /**
     * @return string
     */
    public function getSalespersonId(): ?string
    {
        $order = $this->orderInfo->getOrder();
        return $order->getData('salesperson_id');
    }
}
