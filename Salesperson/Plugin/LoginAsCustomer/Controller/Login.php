<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Plugin\LoginAsCustomer\Controller;

use Magento\Framework\Controller\ResultInterface;
use Magento\LoginAsCustomerFrontendUi\Controller\Login\Index as MagentoLogin;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Framework\Controller\Result\RedirectFactory as ResultRedirectFactory;
use FactorBlue\Salesperson\Model\Config as SalespersonConfig;

class Login
{
    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    /**
     * @var ResultRedirectFactory
     */
    private $resultRedirectFactory;

    /**
     * @var SalespersonConfig
     */
    private $salespersonConfig;

    /**
     * @param CustomerSession $customerSession
     * @param TokenModelFactory $tokenModelFactory
     * @param ResultRedirectFactory $resultRedirectFactory
     * @param SalespersonConfig $salespersonConfig
     */
    public function __construct(
        CustomerSession $customerSession,
        TokenModelFactory $tokenModelFactory,
        ResultRedirectFactory $resultRedirectFactory,
        SalespersonConfig $salespersonConfig
    ) {
        $this->customerSession = $customerSession;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->salespersonConfig = $salespersonConfig;
    }

    /**
     * @param MagentoLogin $subject
     * @param ResultInterface $result
     * @return ResultInterface
     */
    public function afterExecute(MagentoLogin $subject, ResultInterface $result): ResultInterface
    {
        if (!$this->salespersonConfig->isSalespersonLoginEnabled()) {
            return $result;
        }

        $customer = $this->customerSession->getCustomer();
        $customerId = $customer->getId();

        if ($customerId) {
            $token = $this->tokenModelFactory->create()->createCustomerToken($customerId)->getToken();
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setUrl($this->salespersonConfig->getVsfUrl() . '?token=' . $token);
            return $resultRedirect;
        }

        return $result;
    }
}
