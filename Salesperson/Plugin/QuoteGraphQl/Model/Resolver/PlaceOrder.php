<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Plugin\QuoteGraphQl\Model\Resolver;

use Magento\QuoteGraphQl\Model\Resolver\PlaceOrder as MagentoPlaceOrder;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use FactorBlue\Salesperson\Model\SalespersonDataObject;

class PlaceOrder
{
    /**
     * @var SalespersonDataObject
     */
    private $salespersonDataObject;

    /**
     * @param SalespersonDataObject $salespersonDataObject
     */
    public function __construct(
        SalespersonDataObject $salespersonDataObject
    ) {
        $this->salespersonDataObject = $salespersonDataObject;
    }

    /**
     * @param MagentoPlaceOrder $subject
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws GraphQlInputException
     */
    public function beforeResolve(
        MagentoPlaceOrder $subject,
        Field $field,
        ContextInterface $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        if (!isset($args['input']['placed_by_salesperson'])) {
            throw new GraphQlInputException(__('Required parameter placed_by_salesperson is missing.'));
        }

        if ($args['input']['placed_by_salesperson'] === 1 && empty($args['input']['salesperson_id'])) {
            throw new GraphQlInputException(__('Please provide salesperson_id parameter.'));
        }

        if ($args['input']['placed_by_salesperson'] === 0 && !empty($args['input']['salesperson_id'])) {
            throw new GraphQlInputException(__('The salesperson_id parameter is redundant.'));
        }

        if ($args['input']['placed_by_salesperson'] !== 0 && $args['input']['placed_by_salesperson'] !== 1) {
            throw new GraphQlInputException(__('Parameter placed_by_salesperson should be 0 or 1.'));
        }

        $this->salespersonDataObject->setPlacedBySalesperson($args['input']['placed_by_salesperson']);
        $this->salespersonDataObject->setSalespersonId($args['input']['salesperson_id'] ?? '');

        return [$field, $context, $info, $value, $args];
    }
}
