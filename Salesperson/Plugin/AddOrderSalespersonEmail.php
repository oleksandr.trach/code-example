<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Plugin;

use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use FactorBlue\Salesperson\Model\SalespersonDataObject;
use Magento\User\Model\UserFactory;
use Magento\User\Model\ResourceModel\User as UserResource;

class AddOrderSalespersonEmail
{
    /**
     * @var SalespersonDataObject
     */
    private $salespersonDataObject;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserResource
     */
    private $userResource;

    /**
     * @param SalespersonDataObject $salespersonDataObject
     * @param UserFactory $userFactory
     * @param UserResource $userResource
     */
    public function __construct(
        SalespersonDataObject $salespersonDataObject,
        UserFactory $userFactory,
        UserResource $userResource
    ) {
        $this->salespersonDataObject = $salespersonDataObject;
        $this->userFactory = $userFactory;
        $this->userResource = $userResource;
    }

    /**
     * @param OrderIdentity $subject
     * @param $result
     * @return mixed
     */
    public function afterGetEmailCopyTo(OrderIdentity $subject, $result)
    {
        $user = $this->userFactory->create();
        $this->userResource->load($user, $this->salespersonDataObject->getSalespersonId(), 'representive_id');

        if ($user->getId()) {
            if ($result === false) {
                $result = [];
            }

            $result[] = $user->getEmail();
        }

        return $result;
    }
}
