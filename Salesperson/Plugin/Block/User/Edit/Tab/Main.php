<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Plugin\Block\User\Edit\Tab;

use Magento\User\Block\User\Edit\Tab\Main as MagentoMainTab;
use Magento\Framework\Registry;

class Main
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @param Registry $registry
     */
    public function __construct(
        Registry $registry
    ) {
        $this->registry = $registry;
    }

    /**
     * @param MagentoMainTab $subject
     * @return void
     */
    public function beforeGetFormHtml(MagentoMainTab $subject): void
    {
        $model = $this->registry->registry('permissions_user');
        $form = $subject->getForm();
        $baseFieldset = $form->getElement('base_fieldset');
        $baseFieldset->addField(
            'representive_id',
            'text',
            [
                'name' => 'representive_id',
                'label' => __('Representative ID'),
                'id' => 'representive_id',
                'title' => __('Representative ID'),
                'required' => false
            ]
        );
        $form->addValues(['representive_id' => $model->getData('representive_id')]);
        $subject->setForm($form);
    }
}
