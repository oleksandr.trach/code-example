<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Plugin\Order;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\Data\OrderInterface;

class SalespersonExtensionAttributes
{
    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param Order $order
     * @return Order
     */
    public function afterGet(OrderRepositoryInterface $orderRepository, Order $order): Order
    {
        $this->setOrderSalespersonInformation($order);
        return $order;
    }

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderSearchResultInterface $orderSearchResult
     * @return OrderSearchResultInterface
     */
    public function afterGetList(
        OrderRepositoryInterface $orderRepository,
        OrderSearchResultInterface $orderSearchResult
    ): OrderSearchResultInterface {
        foreach ($orderSearchResult->getItems() as $order) {
            $this->setOrderSalespersonInformation($order);
        }

        return $orderSearchResult;
    }

    /**
     * @param OrderInterface $order
     * @return void
     */
    private function setOrderSalespersonInformation(OrderInterface $order): void
    {
        $extensionAttributes = $order->getExtensionAttributes();

        if ($extensionAttributes) {
            $extensionAttributes->setPlacedBySalesperson($order->getData('placed_by_salesperson'));
            $extensionAttributes->setSalespersonId($order->getData('salesperson_id'));
        }
    }
}
