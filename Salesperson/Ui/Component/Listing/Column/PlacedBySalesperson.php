<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use FactorBlue\Salesperson\Model\Order\PlacedBySalesperson\ValueResolver as PlacedBySalespersonValueResolver;

class PlacedBySalesperson extends Column
{
    /**
     * @var PlacedBySalespersonValueResolver
     */
    private $placedBySalespersonValueResolver;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PlacedBySalespersonValueResolver $placedBySalespersonValueResolver
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        PlacedBySalespersonValueResolver $placedBySalespersonValueResolver,
        array $components = [],
        array $data = []
    ) {
        $this->placedBySalespersonValueResolver = $placedBySalespersonValueResolver;
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $value = $item[$this->getData('name')];
                $item[$this->getData('name')] = $this->placedBySalespersonValueResolver->getValue($value);
            }
        }

        return $dataSource;
    }
}
