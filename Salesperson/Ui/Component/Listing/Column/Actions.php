<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Ui\Component\Listing\Column;

use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class Actions extends Column
{
    private $urlBuilder;

    private $customerRepository;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        CustomerRepositoryInterface $customerRepository,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->customerRepository = $customerRepository;
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $customerId = (int) $item['entity_id'];

                try {
                    $customer = $this->customerRepository->getById($customerId);
                } catch (LocalizedException $e) {
                    break;
                }

                $customerActiveCustomAttribute = $customer->getCustomAttribute('customer_active');

                if (!$customerActiveCustomAttribute) {
                    break;
                }

                $customerActive = (int) $customerActiveCustomAttribute->getValue();

                $noActiveContent = 'Sorry, your account is in the process of approval.';
                $noActiveContent .= ' You will be notified via an email once the account is activated';

                $item[$this->getData('name')]['login'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'loginascustomer/login/login',
                        ['customer_id' => $item['entity_id']]
                    ),
                    'label' => __('Login'),
                    'title' => __('Login as Customer'),
                    'content' => __('You are about to be logged in as a customer.'),
                    'customer_active' => $customerActive,
                    'not_active_content' => __($noActiveContent),
                    'error' => __('Something went wrong while executing request.')
                ];
            }
        }

        return $dataSource;
    }
}
