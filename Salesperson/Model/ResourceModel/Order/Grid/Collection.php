<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Model\ResourceModel\Order\Grid;

use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as MagentoOrderGridCollection;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;
use Magento\Backend\Model\Auth\Session as AuthSession;
use FactorBlue\Salesperson\Model\Config;

class Collection extends MagentoOrderGridCollection
{
    /**
     * @var AuthSession
     */
    private $authSession;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param AuthSession $authSession
     * @param Config $config
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        AuthSession $authSession,
        Config $config
    ) {
        $this->authSession = $authSession;
        $this->config = $config;
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager
        );
    }

    /**
     * @return Collection
     */
    protected function _initSelect(): Collection
    {
        parent::_initSelect();
        $salespersonRoleId = $this->config->getSalespersonRoleId();
        $adminUser = $this->authSession->getUser();
        $adminUserAclRole = (int) $adminUser->getAclRole();

        if ($salespersonRoleId === $adminUserAclRole) {
            $this->getSelect()->where('salesperson_id = ?', $adminUser->getData('representive_id'));
        }

        return $this;
    }
}
