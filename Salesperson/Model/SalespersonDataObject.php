<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Model;

use Magento\Framework\DataObject;

class SalespersonDataObject extends DataObject
{
    /**
     * @return int
     */
    public function getPlacedBySalesperson(): int
    {
        $value = $this->getData('placed_by_salesperson');
        return $value ?? 0;
    }

    /**
     * @param int $placedBySalesperson
     * @return void
     */
    public function setPlacedBySalesperson(int $placedBySalesperson): void
    {
        $this->setData('placed_by_salesperson', $placedBySalesperson);
    }

    /**
     * @return string
     */
    public function getSalespersonId(): string
    {
        $value = $this->getData('salesperson_id');
        return $value ?? '';
    }

    /**
     * @param string $salespersonId
     * @return void
     */
    public function setSalespersonId(string $salespersonId): void
    {
        $this->setData('salesperson_id', $salespersonId);
    }
}
