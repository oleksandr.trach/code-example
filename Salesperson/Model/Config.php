<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    public const XML_PATH_IS_ENABLED    = 'salesperson/login_as_customer/is_salesperson_login_enabled';
    public const XML_PATH_VSF_URL       = 'salesperson/login_as_customer/vsf_url';
    public const XML_PATH_ROLE          = 'salesperson/login_as_customer/salesperson_role';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isSalespersonLoginEnabled(): bool
    {
        return (bool)$this->scopeConfig->isSetFlag(self::XML_PATH_IS_ENABLED);
    }

    /**
     * @return string
     *
     * @throws LocalizedException
     */
    public function getVsfUrl(): string
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_VSF_URL);
        if (empty($value)) {
            throw new LocalizedException(__('VSF URL must be configured'));
        }

        return $value;
    }

    /**
     * @return int
     */
    public function getSalespersonRoleId(): int
    {
        return (int) $this->scopeConfig->getValue(self::XML_PATH_ROLE);
    }

}
