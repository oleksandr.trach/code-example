<?php
declare(strict_types=1);

namespace FactorBlue\Salesperson\Model\Order\PlacedBySalesperson;

class ValueResolver
{
    /**
     * @param string $value
     * @return string
     */
    public function getValue(string $value): string
    {
        $value = (int) $value;
        $valueMap = [0 => __('No'), 1 => __('Yes')];
        $phrase = $valueMap[$value] ?? __('Undefined');
        return $phrase->getText();
    }
}
