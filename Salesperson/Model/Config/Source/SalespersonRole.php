<?php

namespace FactorBlue\Salesperson\Model\Config\Source;

use Magento\Authorization\Model\ResourceModel\Role\CollectionFactory as RoleCollectionFactory;
use Magento\Authorization\Model\ResourceModel\Role\Collection as RoleCollection;
use Magento\Framework\Data\OptionSourceInterface;

class SalespersonRole implements  OptionSourceInterface
{

    /**
     * @var array
     */
    private $options;

    private RoleCollectionFactory $roleCollectionFactory;

    /**
     * @param RoleCollectionFactory $roleCollectionFactory
     */
    public function __construct(
        RoleCollectionFactory $roleCollectionFactory
    )
    {
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    public function toOptionArray(): array
    {
        if ($this->options !== null) {
            return $this->options;
        }

        /** @var RoleCollection $roleCollection */
        $roleCollection = $this->roleCollectionFactory->create();
        $roleCollection->setRolesFilter();

        return ($this->options = $roleCollection->toOptionArray());
    }
}
