define([
    'Magento_Ui/js/grid/columns/column',
    'Magento_Ui/js/modal/confirm',
    'jquery',
    'underscore',
    'mage/translate'
], function (Column, confirm, $, _, $t) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'FactorBlue_Salesperson/grid/cells/login'
        },

        getLabel: function (row) {
            return row[this.index]['login']['label'];
        },

        isActionData: function (row) {
            return row[this.index];
        },

        preview: function (row, url) {
            let ajaxError = row[this.index]['login']['error'];

            confirm({
                title: row[this.index]['login']['title'],
                content: this.isCustomerActive(row) ? row[this.index]['login']['content'] : row[this.index]['login']['not_active_content'],
                modalClass: 'confirm lac-confirm',
                actions: {
                    confirm: function () {
                        var formKey = $('input[name="form_key"]').val(),
                        params = {};

                        if (formKey) {
                            params.form_key = formKey;
                        }

                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            data: params,
                            showLoader: true,

                            success: function (data) {
                                var messages = data.messages || [];

                                if (data.message) {
                                    messages.push(data.message);
                                }

                                if (data.redirectUrl) {
                                    window.open(data.redirectUrl);
                                } else if (messages.length) {
                                    messages = messages.map(function (message) {
                                        return _.escape(message);
                                    });

                                    alert(messages.join('<br>'));
                                }
                            },

                            error: function (jqXHR) {
                                alert(ajaxError);
                            }
                        });
                    }
                },
                buttons: this.getButtons(row)
            });

            return false;
        },

        getButtons: function (row) {
            var buttons = [{
                    text: $t('Cancel'),
                    class: 'action-secondary action-dismiss',

                    click: function (event) {
                        this.closeModal(event);
                    }
                }];

            if (this.isCustomerActive(row)) {
                buttons.push(
                    {
                        text: $t('Login as Customer'),
                        class: 'action-primary action-accept',

                        click: function (event) {
                            this.closeModal(event, true);
                        }
                    }
                );
            }
            return buttons;
        },

        isCustomerActive: function (row) {
            return row[this.index]['login']['customer_active'] === 1
        },

        getFieldHandler: function (row) {
            if (!this.isActionData(row)) {
                return;
            }

            var url = row[this.index]['login']['href'];
            return this.preview.bind(this, row, url);
        }
    });
});
