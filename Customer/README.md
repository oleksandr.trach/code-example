# Factor Blue Customer Module

The Factor Blue Customer module extends Magento Customer module - adding new attributes for update from Exact and display using GraphQL endpoint.

## GraphQL Customer Data

### Attributes

The following customer attributes has been added:

* Exact Customer Is Active (customer_active)
* Debtor ID Exact Globe (exact_debtor_id)
* Salesperson Name (salesperson_name)
* Representative ID (representive_id)
* Payment Conditions (payment_conditions)

## GraphQL examples

### Obtain customer token - mutation
```
mutation {
  generateCustomerToken(
    email: "vesna@factorblue.com"
    password: "12QWaszx!@"
  ) {
    token
  }
}
```

### Mutation response
```
{
  "data": {
    "generateCustomerToken": {
      "token": "0ydje56en7tkzctfcromkfo3fm66wqn2"
    }
  }
}
```

### Fetch certain customer's data - add previously generated token to the request header
```
Authorization: Bearer 0ydje56en7tkzctfcromkfo3fm66wqn2 
```

### Customer data query
```
{
  customer {
    firstname
    lastname
    email
    customer_active
    exact_debtor_id
    salesperson_name
    representive_id
    payment_conditions
  }
}
```

### Customer query response
```
{
  "data": {
    "customer": {
      "firstname": "Vesna",
      "lastname": "Slavkovic",
      "email": "vesna@factorblue.com",
      "customer_active": 1,
      "exact_debtor_id": "123123123",
      "salesperson_name": "Vesna",
      "representive_id": "999999",
      "payment_conditions": "2"
    }
  }
}
```

### Create inactive customer account
```
mutation{
  createCustomer(input: {
    firstname: "Vesna", lastname: "Slavkovic",
    email: "vesna.slavkovic@factorblue.com", password: "12QWaszx!@",
  	customer_active: 0}
  ){
    customer{
      firstname,
      lastname
    }
  }
}
```

### REST API update customer data example

![Update customer](docs/update_customer_rest.png?raw=true "Update customer")
