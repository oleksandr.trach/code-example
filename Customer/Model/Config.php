<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;

class Config
{
    const XML_PATH_COMPANY_TYPES = 'customer_configuration/address/company_types';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Json
     */
    private $json;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Json $json
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Json $json
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->json = $json;
    }

    /**
     * @return array
     */
    public function getCompanyTypes(): array
    {
        $result = [];
        $value = $this->scopeConfig->getValue(self::XML_PATH_COMPANY_TYPES) ?? '[]';
        $options = $this->json->unserialize($value);

        foreach ($options as $option) {
            $optionValues = array_values($option);
            $result[] = array_shift($optionValues);
        }

        return $result;
    }
}
