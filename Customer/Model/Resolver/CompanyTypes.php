<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use FactorBlue\Customer\Model\Config as CustomerConfig;

class CompanyTypes implements ResolverInterface
{
    /**
     * @var CustomerConfig
     */
    private $customerConfig;

    /**
     * @param CustomerConfig $customerConfig
     */
    public function __construct(
        CustomerConfig $customerConfig
    ) {
        $this->customerConfig = $customerConfig;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        return ['types' => $this->customerConfig->getCompanyTypes()];
    }
}
