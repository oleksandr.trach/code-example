<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Model\Resolver;

use Magento\CustomerGraphQl\Model\Customer\Address\CreateCustomerAddress as CreateCustomerAddressModel;
use Magento\CustomerGraphQl\Model\Customer\CreateCustomerAccount;
use Magento\CustomerGraphQl\Model\Customer\ExtractCustomerData;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Newsletter\Model\Config;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\LoginAsCustomerAssistance\Api\IsAssistanceEnabledInterface;

class CreateCustomer implements ResolverInterface
{
    /**
     * @var ExtractCustomerData
     */
    private $extractCustomerData;

    /**
     * @var CreateCustomerAccount
     */
    private $createCustomerAccount;

    /**
     * @var Config
     */
    private $newsLetterConfig;

    /**
     * @var CreateCustomerAddressModel
     */
    private $createCustomerAddress;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @param ExtractCustomerData $extractCustomerData
     * @param CreateCustomerAccount $createCustomerAccount
     * @param Config $newsLetterConfig
     * @param CreateCustomerAddressModel $createCustomerAddress
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        ExtractCustomerData $extractCustomerData,
        CreateCustomerAccount $createCustomerAccount,
        Config $newsLetterConfig,
        CreateCustomerAddressModel $createCustomerAddress,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->extractCustomerData = $extractCustomerData;
        $this->createCustomerAccount = $createCustomerAccount;
        $this->newsLetterConfig = $newsLetterConfig;
        $this->createCustomerAddress = $createCustomerAddress;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws GraphQlInputException
     * @throws LocalizedException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null): array
    {
        if (empty($args['input']) || !is_array($args['input'])) {
            throw new GraphQlInputException(__('"input" value should be specified.'));
        }

        if (!$this->newsLetterConfig->isActive(ScopeInterface::SCOPE_STORE)) {
            $args['input']['is_subscribed'] = false;
        }

        if (isset($args['input']['date_of_birth'])) {
            $args['input']['dob'] = $args['input']['date_of_birth'];
        }

        /**
         * This double validation is needed in order to prevent customer registration if
         * customer address validation is failed.
         */
        if (isset($args['input']['country_code'])) {
            $args['input']['country_id'] = $args['input']['country_code'];
        }
        $this->createCustomerAddress->validateData($args['input']);

        // Set allow remote assistance;
        $args['input']['extension_attributes']['assistance_allowed'] = IsAssistanceEnabledInterface::ALLOWED;

        $customer = $this->createCustomerAccount->execute(
            $args['input'],
            $context->getExtensionAttributes()->getStore()
        );

        // Unset allow remote assistance
        unset($args['input']['extension_attributes']);
        $this->createCustomerAddress->execute((int)$customer->getId(), $args['input']);

        /**
         * We need to load the customer object again so the customer data contains newly created address.
         */
        $customer = $this->customerRepository->getById((int)$customer->getId());
        $data = $this->extractCustomerData->execute($customer);

        return ['customer' => $data];
    }
}
