<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\EmailNotification as MagentoEmailNotification;
use Magento\Framework\Exception\LocalizedException;

class EmailNotification extends MagentoEmailNotification
{
    /**
     * Check if the customer is active. If not then do not send an E-mail notification.
     *
     * @param CustomerInterface $customer
     * @param string $type
     * @param string $backUrl
     * @param int|null $storeId
     * @param string $sendemailStoreId
     * @return void
     * @throws LocalizedException
     */
    public function newAccount(
        CustomerInterface $customer,
        $type = self::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = null,
        $sendemailStoreId = null
    ): void {
        $customerActiveCustomAttribute = $customer->getCustomAttribute('customer_active');

        if ($customerActiveCustomAttribute) {
            $customerActive = (int)$customerActiveCustomAttribute->getValue();

            if ($customerActive) {
                parent::newAccount($customer, $type, $backUrl, $storeId, $sendemailStoreId);
            }
        }
    }
}
