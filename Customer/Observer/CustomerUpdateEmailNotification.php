<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\Exception\LocalizedException;

class CustomerUpdateEmailNotification implements ObserverInterface
{
    /**
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @param EmailNotificationInterface $emailNotification
     */
    public function __construct(
        EmailNotificationInterface $emailNotification
    ) {
        $this->emailNotification = $emailNotification;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer): void
    {
        $customerDataObject = $observer->getCustomerDataObject();
        $origCustomerDataObject = $observer->getOrigCustomerDataObject();

        if ($customerDataObject && $origCustomerDataObject) {
            $customerActiveCustomAttribute = $customerDataObject->getCustomAttribute('customer_active');
            $origCustomerActiveCustomAttribute = $origCustomerDataObject->getCustomAttribute('customer_active');

            if ($customerActiveCustomAttribute && $origCustomerActiveCustomAttribute) {
                $origCustomerActive = (int)$origCustomerActiveCustomAttribute->getValue();
                $customerActive = (int)$customerActiveCustomAttribute->getValue();

                if ($origCustomerActive !== $customerActive && $customerActive === 1) {
                    $this->emailNotification->newAccount($customerDataObject);
                }
            }
        }
    }
}
