<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use FactorBlue\Customer\Exception\CustomerActiveException;

class CustomerActive implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return void
     * @throws CustomerActiveException
     */
    public function execute(Observer $observer): void
    {
        $customer = $observer->getEvent()->getData('model');

        if ($customer) {
            $customerActive = $customer->getData('customer_active');

            if (is_scalar($customerActive)) {
                $customerActive = intval($customerActive);

                if (!$customerActive) {
                    throw new CustomerActiveException(__('Customer is not active.'));
                }
            }
        }
    }
}
