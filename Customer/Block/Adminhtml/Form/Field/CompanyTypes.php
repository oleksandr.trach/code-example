<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class CompanyTypes extends AbstractFieldArray
{
    /**
     * @return void
     */
    protected function _prepareToRender(): void
    {
        $this->addColumn('company_type', ['label' => __('Company Type')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Company Type');
    }
}
