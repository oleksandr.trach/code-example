<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Exception;

use Magento\Framework\Exception\AuthenticationException;

class CustomerActiveException extends AuthenticationException
{
}
