<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Logger\Handler;

use Magento\Framework\Logger\Handler\Base as BaseHandler;
use Monolog\Logger as MonologLogger;

class ErrorHandler extends BaseHandler
{
    protected $loggerType = MonologLogger::ERROR;

    protected $fileName = '/var/log/factorblue.customer.log';
}
