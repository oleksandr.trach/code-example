<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Psr\Log\LoggerInterface;
use Exception;

class CompanyTypeAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeResource $attributeResource
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeResource $attributeResource,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeResource = $attributeResource;
        $this->logger = $logger;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply(): void
    {
        $this->moduleDataSetup->startSetup();

        try {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
            $customerSetup->addAttribute(Customer::ENTITY, 'company_type', [
                'label' => 'Company Type',
                'position' => 250,
                'required' => false,
                'system' => false,
                'user_defined' => true
            ]);
            $defaultAttributeSetId = $customerSetup->getDefaultAttributeSetId(Customer::ENTITY);
            $defaultAttributeGroupId = $customerSetup->getDefaultAttributeGroupId(Customer::ENTITY);
            $customerSetup->addAttributeToGroup(
                Customer::ENTITY,
                $defaultAttributeSetId,
                $defaultAttributeGroupId,
                'company_type'
            );
            $eavConfig = $customerSetup->getEavConfig();
            $attribute = $eavConfig->getAttribute(Customer::ENTITY, 'company_type');
            $attribute->setData('used_in_forms', ['adminhtml_customer']);
            $this->attributeResource->save($attribute);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        $this->moduleDataSetup->endSetup();
    }
}
