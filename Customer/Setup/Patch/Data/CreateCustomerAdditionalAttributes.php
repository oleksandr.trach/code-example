<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Psr\Log\LoggerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Exception;

class CreateCustomerAdditionalAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * @var array[]
     */
    private $attributesData = [
        ['code' => 'kvk_number', 'label' => 'KVK Number', 'position' => 260],
        ['code' => 'function', 'label' => 'Function', 'position' => 270],
        ['code' => 'btw_number', 'label' => 'BTW Number', 'position' => 280]
    ];

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeResource $attributeResource
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeResource $attributeResource,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeResource = $attributeResource;
        $this->logger = $logger;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply(): void
    {
        $this->moduleDataSetup->startSetup();

        try {
            $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

            foreach ($this->attributesData as $attributeData) {
                $this->createAttribute($customerSetup, $attributeData);
            }
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->moduleDataSetup->endSetup();
    }

    /**
     * @param $customerSetup
     * @param array $attributeData
     * @return void
     * @throws AlreadyExistsException
     */
    private function createAttribute($customerSetup, array $attributeData): void
    {
        $customerSetup->addAttribute(Customer::ENTITY, $attributeData['code'], [
            'label' => $attributeData['label'],
            'position' => $attributeData['position'],
            'required' => false,
            'system' => false,
            'user_defined' => true
        ]);
        $defaultAttributeSetId = $customerSetup->getDefaultAttributeSetId(Customer::ENTITY);
        $defaultAttributeGroupId = $customerSetup->getDefaultAttributeGroupId(Customer::ENTITY);
        $customerSetup->addAttributeToGroup(
            Customer::ENTITY,
            $defaultAttributeSetId,
            $defaultAttributeGroupId,
            $attributeData['code']
        );
        $eavConfig = $customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(Customer::ENTITY, $attributeData['code']);
        $attribute->setData('used_in_forms', ['adminhtml_customer']);
        $this->attributeResource->save($attribute);
    }
}
