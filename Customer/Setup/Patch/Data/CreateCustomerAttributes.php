<?php
declare(strict_types=1);

namespace FactorBlue\Customer\Setup\Patch\Data;

use Magento\Customer\Model\Attribute\Backend\Data\Boolean;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Psr\Log\LoggerInterface;
use Zend_Validate_Exception;

class CreateCustomerAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetup
     */
    private $customerSetup;

    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeResource $attributeResource
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeResource $attributeResource,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetup = $customerSetupFactory->create(['setup' => $moduleDataSetup]);
        $this->attributeResource = $attributeResource;
        $this->logger = $logger;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply(): void
    {
        $this->moduleDataSetup->startSetup();

        try {
            $this->createIsActiveAttribute();
            $this->createExactDebtorIdAttribute();
            $this->createSalespersonNameAttribute();
            $this->createRepresentativeIdAttribute();
            $this->createPaymentConditionsAttribute();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        $this->moduleDataSetup->endSetup();
    }

    /**
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function createIsActiveAttribute(): void
    {
        $this->customerSetup->addAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'customer_active',
            [
                'type' => 'static',
                'input' => 'boolean',
                'backend' => Boolean::class,
                'label' => 'Exact Customer Is Active',
                'required' => 0,
                'position' => 200,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0,
                'is_searchable_in_grid' => 0
            ]
        );
        $this->customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'customer_active'
        );
        $eavConfig = $this->customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'customer_active'
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer'
        ]);
        $this->attributeResource->save($attribute);
    }

    /**
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function createExactDebtorIdAttribute(): void
    {
        $this->customerSetup->addAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'exact_debtor_id',
            [
                'type' => 'static',
                'label' => 'Debtor ID Exact Globe',
                'required' => 0,
                'position' => 210,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0,
                'is_searchable_in_grid' => 0,
            ]
        );
        $this->customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'exact_debtor_id'
        );
        $eavConfig = $this->customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'exact_debtor_id'
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer'
        ]);
        $this->attributeResource->save($attribute);
    }

    /**
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function createSalespersonNameAttribute(): void
    {
        $this->customerSetup->addAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'salesperson_name',
            [
                'type' => 'static',
                'label' => 'Salesperson Name',
                'required' => 0,
                'position' => 220,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0,
                'is_searchable_in_grid' => 0,
            ]
        );
        $this->customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'salesperson_name'
        );
        $eavConfig = $this->customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'salesperson_name'
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer'
        ]);
        $this->attributeResource->save($attribute);
    }

    /**
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function createRepresentativeIdAttribute(): void
    {
        $this->customerSetup->addAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'representive_id',
            [
                'type' => 'static',
                'label' => 'Representative ID',
                'required' => 0,
                'position' => 230,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'is_filterable_in_grid' => 1,
                'is_searchable_in_grid' => 1,
            ]
        );
        $this->customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'representive_id'
        );
        $eavConfig = $this->customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'representive_id'
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer'
        ]);
        $this->attributeResource->save($attribute);
    }

    /**
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    private function createPaymentConditionsAttribute(): void
    {
        $this->customerSetup->addAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'payment_conditions',
            [
                'type' => 'static',
                'label' => 'Payment Conditions',
                'required' => 0,
                'position' => 240,
                'system' => 0,
                'user_defined' => 1,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0,
                'is_searchable_in_grid' => 0,
            ]
        );
        $this->customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'payment_conditions'
        );
        $eavConfig = $this->customerSetup->getEavConfig();
        $attribute = $eavConfig->getAttribute(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'payment_conditions'
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer'
        ]);
        $this->attributeResource->save($attribute);
    }
}
